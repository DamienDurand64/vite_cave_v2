import { showToasts } from "../../utils/toastify/toastify";
import Button, { BUTTONCOLOR } from "../button/button";

function Home() {
  return (
    <div className="">
      <Button
        onClick={() => {
          showToasts(`Ravi de vous revoir `, "success");
        }}
        color={BUTTONCOLOR.blue}
      >
        Button
      </Button>
    </div>
  );
}
export default Home;
