import { Navigate } from "react-router-dom";
import { useAuth } from "../../auth/AuthContext";
import { ReactNode } from "react";

export const ProtectedRoute = ({ children }: { children?: ReactNode }) => {
  const { user, isLoading } = useAuth();

  if (isLoading) {
    return <div>Chargement...</div>;
  }

  if (!user) {
    return <Navigate to="/" replace />;
  }

  return <>{children}</>;
};
