import { Link, Outlet } from "react-router-dom";
import { useAuth } from "../../auth/AuthContext";
import Logo from "../layout/Logo";
import FooterIcons, {
  IFooterIconsReturn,
} from "../layout/footerIcons/FooterIcons";
import Logout from "../logout/Logout";
import Button, { BUTTONCOLOR } from "../button/button";
import { signal } from "@preact/signals-react";
import ModalLogin from "../modal/ModalLogin";

const openModalLogin = signal(false);

function Layout() {
  const { user } = useAuth();
  const footerIcons = FooterIcons();
  return (
    <div className="flex flex-col min-h-screen">
      <div className="bg-gray p-4 flex justify-between">
        <div className="flex flex-col">
          <Logo />
        </div>
        <div className="flex flex-col">
          {!user ? (
            <Button
              onClick={() => {
                openModalLogin.value = true;
              }}
              color={BUTTONCOLOR.yellow}
            >
              Connection
            </Button>
          ) : (
            <Logout />
          )}
        </div>
      </div>
      {openModalLogin.value && <ModalLogin openModalLogin={openModalLogin} />}

      <div className="flex-grow p-4 bg-gray-light">
        <Outlet />
      </div>

      <div className="fixed bottom-0 p-4 w-screen flex justify-between bg-gray">
        {footerIcons.map((icon: IFooterIconsReturn) => (
          <Link key={icon.name} to={icon.link}>
            <div className="flex flex-col items-center">
              {icon.icon}
              <span className="text-xs">{icon.name}</span>
            </div>
          </Link>
        ))}
      </div>
    </div>
  );
}

export default Layout;
