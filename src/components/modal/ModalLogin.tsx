import React, { useRef } from "react";
import { Signal } from "@preact/signals-react";
import { LoginForm } from "../login/LoginForm";

interface IModalLoginProps {
  openModalLogin: Signal<boolean>;
}

function ModalLogin({ openModalLogin }: IModalLoginProps) {
  const modalRef = useRef<HTMLDivElement>(null);

  const handleClickOutside = (event: React.MouseEvent) => {
    if (modalRef.current && !modalRef.current.contains(event.target as Node)) {
      openModalLogin.value = false;
    }
  };

  return (
    <div
      className="fixed top-0 left-0 w-full h-full bg-gray z-50 flex justify-center items-center bg-opacity-50"
      onClick={handleClickOutside}
    >
      <LoginForm openModalLogin={openModalLogin} modalref={modalRef} />
    </div>
  );
}

export default ModalLogin;
