import { useAuth } from "../../auth/AuthContext";
import Button, { BUTTONCOLOR } from "../button/button";

function Logout() {
  const { logout } = useAuth();
  return (
    <Button color={BUTTONCOLOR.red} onClick={() => logout()}>
      Logout
    </Button>
  );
}

export default Logout;
