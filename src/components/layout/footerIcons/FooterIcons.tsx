import { useAuth } from "../../../auth/AuthContext";
import { CgProfile } from "react-icons/cg";
import { FaChartBar } from "react-icons/fa";
import { HiOutlineHome } from "react-icons/hi";
import { RiAddCircleFill } from "react-icons/ri";

export interface IFooterIconsReturn {
  name: string;
  icon: JSX.Element;
  link: string;
}

function FooterIcons(): IFooterIconsReturn[] {
  const { user } = useAuth();

  return [
    {
      name: "Home",
      icon: <HiOutlineHome size="2rem" />,
      link: "/",
    },
    {
      name: "Profil",
      icon: user ? (
        <div className="rounded-full  w-8 h-8">
          <img
            src={user.image ?? "/images/wine.png"}
            alt="profile"
            sizes="35px"
          />
        </div>
      ) : (
        <CgProfile size="2rem" style={{}} />
      ),
      link: "profil",
    },
    {
      name: "Stats",
      icon: <FaChartBar size="2rem" />,
      link: "stats",
    },
    {
      name: "Ajouter",
      icon: (
        <RiAddCircleFill
          style={{ backgroundColor: "white", borderRadius: "50%" }}
          size="2rem"
        />
      ),
      link: "addwine",
    },
  ];
}

export default FooterIcons;
