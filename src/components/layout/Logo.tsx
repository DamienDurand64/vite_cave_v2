import { Link } from "react-router-dom";

function Logo() {
  return (
    <Link to="/homepage">
      <div className="flex">
        <h1 className="text-2xl font-extrabold">
          Ma <br /> petite <br />
          Cave.
        </h1>
        <img
          src="/images/dropWine.png"
          alt="Logo of the app"
          width={90}
          height={80}
        />
      </div>
    </Link>
  );
}

export default Logo;
