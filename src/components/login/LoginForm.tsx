import Button, { BUTTONCOLOR } from "../button/button";
import { Signal } from "@preact/signals-react";
import { RefObject } from "react";
import useLoginForm from "./useLoginForm.hook";

interface ILoginForm {
  openModalLogin: Signal<boolean>;
  modalref: RefObject<HTMLDivElement>;
}
export function LoginForm({ openModalLogin, modalref }: ILoginForm) {
  const { handleSubmit, onSubmit, register } = useLoginForm({ openModalLogin });
  return (
    <div
      ref={modalref}
      className="w-full border border-gray-dark mx-4 h-2/3 rounded-xl bg-gray-light flex flex-col items-center p-4 justify-center"
    >
      <h1 className="text-xl font-bold text-center">
        Connectez vous à votre compte
      </h1>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="flex flex-col py-8 gap-1 font-semibold">
          <label htmlFor="email">Email</label>
          <input
            className="border border-gray-dark rounded-lg p-2 mb-4"
            type="text"
            placeholder="Email"
            {...register("Email", { required: true, pattern: /^\S+@\S+$/i })}
          />
          <label htmlFor="password">Mot de passe</label>
          <input
            className="border border-gray-dark rounded-lg p-2"
            type="password"
            placeholder="password"
            {...register("password", { required: true, min: 8 })}
          />
          <div className="mt-4 flex justify-center">
            <Button onClick={() => {}} color={BUTTONCOLOR.yellow}>
              Connection
            </Button>
          </div>
        </div>
      </form>
      <div className="flex flex-col gap-2 mt-10">
        <h2>Vous n'avez pas de compte ?</h2>
        <Button onClick={() => {}} color={BUTTONCOLOR.blue}>
          S'inscrire
        </Button>
      </div>
    </div>
  );
}
