import { useAuth } from "../../auth/AuthContext";
import { useNavigate } from "react-router-dom";
import { useForm } from "react-hook-form";
import { Signal } from "@preact/signals-react";
import { UseFormRegister, UseFormHandleSubmit } from "react-hook-form";

type useLoginFormProps = {
  openModalLogin: Signal<boolean>;
};
type SigninData = {
  Email: string;
  password: string;
};

interface useLoginFormReturn {
  register: UseFormRegister<SigninData>;
  handleSubmit: UseFormHandleSubmit<SigninData, undefined>;
  onSubmit: (data: SigninData) => Promise<void>;
}

export default function useLoginForm({
  openModalLogin,
}: useLoginFormProps): useLoginFormReturn {
  const { login } = useAuth();
  const navigate = useNavigate();

  const { register, handleSubmit, reset } = useForm<SigninData>();

  const onSubmit = async (data: SigninData) => {
    try {
      await login(data.Email, data.password);
      navigate("/profil");
      openModalLogin.value = false;
      reset();
    } catch (error) {}
  };
  return { register, handleSubmit, onSubmit };
}
