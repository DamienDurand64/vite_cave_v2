import { ReactNode } from "react";

type TButtonProps = {
  onClick: () => void;
  children: string | ReactNode;
  color: string;
};

export enum BUTTONCOLOR {
  yellow = "bg-yellow",
  red = "bg-purple",
  blue = "bg-blue",
}

const Button = ({ onClick, children, color }: TButtonProps) => {
  const buttonClassName = `rounded-md px-4 py-1  ${color}`;

  return (
    <button className={buttonClassName} onClick={onClick} type="submit">
      {children}
    </button>
  );
};

export default Button;
