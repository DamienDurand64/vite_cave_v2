import React from "react";
import Button, { BUTTONCOLOR } from "../button/button";

type Props = {};

function Signin({}: Props) {
  return (
    <Button color={BUTTONCOLOR.blue} onClick={() => {}}>
      Signin
    </Button>
  );
}

export default Signin;
