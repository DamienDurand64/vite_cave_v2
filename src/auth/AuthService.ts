import axiosInstance from "../services/axios/axiosInstance";
import { ENDPOINTS } from "../services/endPoints/endPoints";

export const AuthService = {
  login: async (email: string, password: string) => {
    const response = await axiosInstance.post(ENDPOINTS.LOGIN, {
      email,
      password,
    });
    return response.data;
  },
  logout: async () => {
    await axiosInstance.post(ENDPOINTS.LOGOUT);
  },
  fetchCurrentUser: async () => {
    const response = await axiosInstance.get(ENDPOINTS.CURRENT_USER);
    return response.data;
  },
};
