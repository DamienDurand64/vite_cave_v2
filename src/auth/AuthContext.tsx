import {
  ReactNode,
  createContext,
  useContext,
  useState,
  useEffect,
} from "react";
import { AuthService } from "./AuthService";
import { showToasts } from "../utils/toastify/toastify";

interface IUser {
  id: number;
  firstname: string;
  lastname: string;
  email: string;
  birthday: Date;
  image?: string;
}

interface AuthContextType {
  user: IUser | null;
  login: (email: string, password: string) => Promise<void>;
  logout: () => void;
  isLoading: boolean;
}

export const AuthContext = createContext<AuthContextType>(null!);

export const AuthProvider = ({ children }: { children: ReactNode }) => {
  const [user, setUser] = useState(null);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    setIsLoading(true);
    const checkUserAuthentication = async () => {
      try {
        const userResponse = await AuthService.fetchCurrentUser();
        setUser(userResponse);
      } catch (error) {
        console.error("Error checking user authentication:", error);
      }
      setIsLoading(false);
    };

    checkUserAuthentication();
  }, []);

  const login = async (email: string, password: string) => {
    setIsLoading(true);
    try {
      const userResponse = await AuthService.login(email, password);
      setUser(userResponse);
      showToasts(
        `Ravi de vous revoir ${userResponse.firstname} ${userResponse.lastname}`,
        "success"
      );
    } catch (error) {
      showToasts("La tentative de connexion a échoué", "error");
      console.error("Login failed:", error);
    }
    setIsLoading(false);
  };

  const logout = async () => {
    try {
      await AuthService.logout();
      setUser(null);
    } catch (error) {
      console.error("Logout failed:", error);
    }
  };

  return (
    <AuthContext.Provider value={{ user, login, logout, isLoading }}>
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => useContext(AuthContext);
