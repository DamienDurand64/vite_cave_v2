import { ToastOptions, toast, Flip } from "react-toastify";

type ToastType = "error" | "success";

export const showToasts = (
  text: string,
  type: ToastType,
  options?: Partial<ToastOptions>
) => {
  const toastFn = type === "error" ? toast.error : toast.success;
  const toastOptions: ToastOptions = {
    position: "top-left",
    autoClose: 2000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
    theme: "dark",
    transition: Flip,
    icon: toastFn === toast.success ? "👋" : "😢",
    style: {
      width: "250px",
      margin: "4px",
      borderRadius: "6px",
    },
  };
  toastFn(text, { ...toastOptions, ...options });
};
