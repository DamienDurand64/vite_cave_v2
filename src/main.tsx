import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import {
  Route,
  RouterProvider,
  createBrowserRouter,
  createRoutesFromElements,
} from "react-router-dom";
import Layout from "./components/routes/Layout.tsx";
import Profil from "./components/routes/Profil.tsx";
import Home from "./components/routes/Home.tsx";
import Wines from "./components/routes/Wines.tsx";
import { AuthProvider } from "./auth/AuthContext.tsx";
import Stats from "./components/routes/Stats.tsx";
import { ToastContainer } from "react-toastify";

import "react-toastify/dist/ReactToastify.css";

const router = createBrowserRouter(
  createRoutesFromElements(
    <Route path="/" element={<Layout />}>
      <Route index element={<Home />} />
      <Route path="profil" element={<Profil />} />
      <Route path="wines" element={<Wines />} />
      <Route path="stats" element={<Stats />} />
    </Route>
  )
);

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <AuthProvider>
      <RouterProvider router={router} />
      <ToastContainer />
    </AuthProvider>
  </React.StrictMode>
);
